with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "mcexercise-shell";
  buildInputs = [
    cargo
    rustc
    rust-bindgen
    clang
    lhapdf
    lhapdf.pdf_sets.cteq6l1
  ];
  shellHook = ''
    export LIBCLANG_PATH="${clang.cc}/lib"
  '';
}
