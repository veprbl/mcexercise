#![allow(non_snake_case)]

use rand;
use rand::SeedableRng;
use rand::distributions::IndependentSample;
use std::f64::consts::{PI, SQRT_2};


use mcexercise::constants::{ALPHA, GAMMA_Z, G_F, HC2, M_Z, SINTHETAW2};

fn main() {
    let mut rng = rand::XorShiftRng::from_seed([31337, 31337, 31337, 31337]);
    let r_range = rand::distributions::Range::new(-0_f64, 1_f64);
    let cos_range = rand::distributions::Range::new(-1_f64, 1_f64);
    let num_events = 100000000;
    let Qf : f64 = -1.;
    let Vmu : f64 = -0.5 + 2_f64 * SINTHETAW2;
    let Vf : f64 = Vmu;
    let Amu : f64 = -0.5;
    let Af : f64 = Amu;

    let shat = 90_f64.powi(2); /* GeV^2 */

    let k = SQRT_2 * G_F * M_Z.powi(2) / 4_f64 / PI / ALPHA;
    let bwd = (shat - M_Z.powi(2)).powi(2) + (GAMMA_Z * M_Z).powi(2);
    let chi1 = k * shat * (shat - M_Z.powi(2)) / bwd;
    let chi2 = (k * shat).powi(2) / bwd;
    let A_0 = Qf.powi(2) - 2_f64 * Qf * Vmu * Vf * chi1 + (Amu.powi(2) + Vmu.powi(2)) * (Af.powi(2) + Vf.powi(2)) * chi2;
    let A_1 = - 4_f64 * Qf * Amu * Af * chi1 + 8_f64 * Amu * Vmu * Af * Vf * chi2;

    let mut max_w = 0.;
    let mut sumw = 0.;
    let mut sumw2 = 0.;
    for _ in 0..num_events {
        let costheta = cos_range.ind_sample(&mut rng);
        let w = HC2 * (4_f64 * PI) * ALPHA.powi(2) / 4_f64 / shat * (A_0 * (1_f64 + costheta.powi(2)) + A_1 * costheta);
        if w > max_w {
            max_w = w;
        }
        sumw += w;
        sumw2 += w.powi(2);
        /*println!("evt {} {:.2}", i, HC2 * w);*/
    }
    let xsec = sumw / (num_events as f64);
    let sigma = (sumw2 / (num_events as f64) - (sumw / (num_events as f64)).powi(2)).sqrt() / (num_events as f64).sqrt();
    println!("MC cross-section {:.2} +- {:.2} pb", xsec, sigma);
    println!("Analytic cross-section {:.2} pb", HC2 * 4_f64 * PI * ALPHA.powi(2) / 3_f64 / shat * A_0);

    const NBINS : usize = 40;
    const ETA_MIN : f64 = -5.0;
    const ETA_MAX : f64 = 5.0;
    const ETA_BIN : f64 = (ETA_MAX - ETA_MIN) / (NBINS as f64);
    let mut eta_dist : [i64; NBINS] = [0; NBINS];
    let mut forward = 0;
    let mut backward = 0;
    for _ in 0..num_events {
        let costheta = cos_range.ind_sample(&mut rng);
        let w = HC2 * (4_f64 * PI) * ALPHA.powi(2) / 4_f64 / shat * (A_0 * (1_f64 + costheta.powi(2)) + A_1 * costheta);
        assert!(w <= max_w);
        let r = r_range.ind_sample(&mut rng);
        if r > (w / max_w) {
            continue;
        }
        let eta = -((1_f64 - costheta) / (1_f64 + costheta)).ln() / 2_f64;
        let bin = ((eta - ETA_MIN) / ETA_BIN) as usize;
        if bin < NBINS {
            eta_dist[bin] += 1;
        }
        if costheta.is_sign_positive() {
            forward += 1;
        } else {
            backward += 1;
        }
    }

    for y in eta_dist.iter() {
        println!("{}", y);
    }

    let fba = ((forward - backward) as f64) / ((forward + backward) as f64);
    println!("Forward-backward assymetry: {}", fba);
    println!("Analytic forward-backward assymetry: {}", 3_f64 * A_1 / (8_f64 * A_0));
}
