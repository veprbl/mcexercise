#![allow(non_snake_case)]

use rand;
use lhapdf_sys;
#[macro_use] extern crate enum_primitive;
use enum_primitive::FromPrimitive;
use rand::SeedableRng;
use rand::distributions::IndependentSample;
use std::f64::consts::{PI, SQRT_2};
use std::cell::RefCell;
use std::fs::File;
use std::io::Write;


use mcexercise::constants::*;
use mcexercise::hepmc::*;

const NUM_FLAVOURS : i32 = 4;
const NUM_COLORS : i32 = 3;

enum_from_primitive! {
enum Parton {
    D = 1,
    U = 2,
    S = 3,
    C = 4,
    B = 5,
    T = 6,
}
}

fn main() {
    let mut rng = rand::XorShiftRng::from_seed([31337, 31337, 31337, 31337]);
    let phi_meas : f64 = 2. * PI;
    let cos_meas : f64 = 2.;
    let cos_range = rand::distributions::Range::new(-1_f64, 1_f64);
    let phi_range = rand::distributions::Range::new(0_f64, 2_f64 * PI);
    let num_events = 1000000;
    let sqrt_s: f64 = 14000_f64; /* GeV */
    let s : f64 = sqrt_s.powi(2); /* GeV^2 */
    let Qmin : f64 = 60.; /* GeV */
    let rho_min = ((Qmin.powi(2) - M_Z.powi(2)) / GAMMA_Z / M_Z).atan();
    let rho_max = ((s - M_Z.powi(2))            / GAMMA_Z / M_Z).atan();
    let rho_meas = rho_max - rho_min;
    let rho_range = rand::distributions::Range::new(rho_min, rho_max);
    let k = SQRT_2 * G_F * M_Z.powi(2) / 4_f64 / PI / ALPHA;
    let pdf = lhapdf_sys::PDF::new("cteq6l1", 0);

    let mut sumw : f64 = 0.;
    let mut sumw2 : f64 = 0.;

    let mut e: Event<'_>;
    let v: Vertex<'_> = Vertex {
        barcode: -1,
        particles_in: vec![],
        particles_out: vec![],
    };
    e = Event {
        event_number: 0,
        scale: 0_f64,
        weight: 0_f64,
        cross_section: 0_f64,
        cross_section_error: 0_f64,
        particles: vec![
            Default::default(),
            Default::default(),
            Default::default(),
            Default::default(),
            ],
        vertices: vec![RefCell::new(v)],
    };
    let p1 = &e.particles[0]; /* incoming hadron */
    let p2 = &e.particles[1]; /* incoming hadron */
    let p3 = &e.particles[2]; /* outgoing lepton */
    let p4 = &e.particles[3]; /* outgoing lepton */

    e.vertices[0].borrow_mut().particles_in.push(p1);
    p1.borrow_mut().end_vertex = Some(&e.vertices[0]);
    e.vertices[0].borrow_mut().particles_in.push(p2);
    p2.borrow_mut().end_vertex = Some(&e.vertices[0]);
    e.vertices[0].borrow_mut().particles_out.push(p3);
    p3.borrow_mut().production_vertex = Some(&e.vertices[0]);
    e.vertices[0].borrow_mut().particles_out.push(p4);
    p4.borrow_mut().production_vertex = Some(&e.vertices[0]);

    p1.borrow_mut().barcode = 10001;
    p2.borrow_mut().barcode = 10002;
    p3.borrow_mut().barcode = 10003;
    p4.borrow_mut().barcode = 10004;

    p1.borrow_mut().momentum.t = sqrt_s / 2_f64;
    p1.borrow_mut().momentum.z = sqrt_s / 2_f64;
    p1.borrow_mut().pdg_id = PDG_ID_PROTON;
    p1.borrow_mut().status = HEPEVT_STATUS_INCOMING_BEAM;
    p2.borrow_mut().momentum.t = sqrt_s / 2_f64;
    p2.borrow_mut().momentum.z = -sqrt_s / 2_f64;
    p2.borrow_mut().status = HEPEVT_STATUS_INCOMING_BEAM;
    p2.borrow_mut().pdg_id = PDG_ID_PROTON;
    p3.borrow_mut().pdg_id = PDG_ID_MU;
    p3.borrow_mut().status = HEPEVT_STATUS_FINAL_STATE;
    p4.borrow_mut().pdg_id = -PDG_ID_MU;
    p4.borrow_mut().status = HEPEVT_STATUS_FINAL_STATE;

    let mut outfile = File::create("output.hepmc").unwrap();

    for i in 1..(num_events+1) {
        let rho = rho_range.ind_sample(&mut rng);
        let shat = M_Z.powi(2) + GAMMA_Z * M_Z * rho.tan();
        let sqrt_shat = shat.sqrt();
        assert!(shat >= Qmin.powi(2));
        assert!(shat <= s);
        let xi0 = rho.cos().powi(2) * GAMMA_Z * M_Z / s;
        let xi1 = k * shat * (shat - M_Z.powi(2)) / s / GAMMA_Z / M_Z; /* xi differs from chi because we replaced integration variable */
        let xi2 = (k * shat).powi(2) / s / GAMMA_Z / M_Z;
        let costheta = cos_range.ind_sample(&mut rng);
        let sintheta = (1_f64 - costheta.powi(2)).sqrt();
        let phi = phi_range.ind_sample(&mut rng);
        let tau = shat / s;
        let y_max = -0.5 * tau.ln();
        let y_meas = 2. * y_max;
        let y_range = rand::distributions::Range::new(-y_max, y_max);
        let y = y_range.ind_sample(&mut rng);
        let x1 = tau.sqrt() * (y).exp();
        let x2 = tau.sqrt() * (-y).exp();
        assert!((x1 * x2 - tau) < 1e-5);

        let mut lw : f64 = 0.;

        for q in 1..(NUM_FLAVOURS + 1) {
            let Vmu : f64 = -0.5 + 2_f64 * SINTHETAW2;
            let Amu : f64 = -0.5;
            let Qf : f64 = match Parton::from_i32(q).unwrap() {
                Parton::U | Parton::C | Parton::T => 2. / 3.,
                Parton::D | Parton::S | Parton::B => -1. / 3.,
            };
            let Vf : f64 = match Parton::from_i32(q).unwrap() {
                Parton::U | Parton::C | Parton::T => 0.5 - 4. / 3. * SINTHETAW2,
                Parton::D | Parton::S | Parton::B => -0.5 + 2. / 3. * SINTHETAW2, /* XXX wrong sign in the original tutorial */
            };
            let Af : f64 = match Parton::from_i32(q).unwrap() {
                Parton::U | Parton::C | Parton::T => 0.5,
                Parton::D | Parton::S | Parton::B => -0.5,
            };
            let B_0 = Qf.powi(2) * xi0 - 2_f64 * Qf * Vmu * Vf * xi1 + (Amu.powi(2) + Vmu.powi(2)) * (Af.powi(2) + Vf.powi(2)) * xi2;
            let B_1 = - 4_f64 * Qf * Amu * Af * xi1 + 8_f64 * Amu * Vmu * Af * Vf * xi2;
            let f1 = pdf.xfx_q2(q, x1, shat) / x1;
            let f2 = pdf.xfx_q2(-q, x2, shat) / x2;
            lw += (B_0 * (1_f64 + costheta.powi(2)) + B_1 * costheta) * f1 * f2;
        }

        /* FIXME virtuality? */
        let p3_com = LorentzVector {
            t: sqrt_shat / 2_f64,
            x: sqrt_shat / 2_f64 * sintheta * phi.cos(),
            y: sqrt_shat / 2_f64 * sintheta * phi.sin(),
            z: sqrt_shat / 2_f64 * costheta,
        };
        let p4_com = LorentzVector {
            t: p3_com.t,
            x: -p3_com.x,
            y: -p3_com.y,
            z: -p3_com.z,
        };
        let beta = (x2 - x1) / (x2 + x1);
        p3.borrow_mut().momentum = p3_com.boost_z(beta);
        p4.borrow_mut().momentum = p4_com.boost_z(beta);

        e.event_number = i;
        e.scale = shat;
        e.weight = 2. * HC2 * phi_meas * cos_meas * rho_meas * y_meas * ALPHA.powi(2) / 4_f64 / shat * lw / (NUM_COLORS as f64);
        sumw += e.weight;
        sumw2 += e.weight.powi(2);
        e.cross_section = sumw / (i as f64);
        e.cross_section_error = (sumw2 / (i as f64) - (sumw / (i as f64)).powi(2)).sqrt() / (i as f64).sqrt();
        print!("MC cross-section {:.2} +- {:.2} pb        \r", e.cross_section, e.cross_section_error);

        outfile.write_all(encode_event(&e).as_bytes()).unwrap();
    }
    println!("MC cross-section {:.2} +- {:.2} pb", e.cross_section, e.cross_section_error);
}
