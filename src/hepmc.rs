use std::cell::RefCell;
use std::ops::Deref;

#[derive(Default)]
pub struct LorentzVector {
    pub t: f64,
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl LorentzVector {
    pub fn boost_z(&self, beta: f64) -> Self {
        let gamma = 1_f64 / (1_f64 - beta.powi(2)).sqrt();
        LorentzVector {
            t: gamma * (self.t - beta * self.z),
            x: self.x,
            y: self.y,
            z: gamma * (-beta * self.t + self.z),
        }
    }
}

pub const HEPEVT_STATUS_FINAL_STATE: u32 = 1;
pub const HEPEVT_STATUS_INCOMING_BEAM: u32 = 4;

#[derive(Default)]
pub struct Particle<'a> {
    pub momentum: LorentzVector,
    pub generated_mass: f64,
    pub pdg_id: i32,
    pub barcode: i32,
    pub status: u32,
    pub/*FIXME*/ end_vertex: Option<&'a RefCell<Vertex<'a>>>,
    pub/*FIXME*/ production_vertex: Option<&'a RefCell<Vertex<'a>>>,
}

pub struct Vertex<'a> {
    pub barcode: i32,
    pub/*FIXME*/ particles_in: Vec<&'a RefCell<Particle<'a>>>,
    pub/*FIXME*/ particles_out: Vec<&'a RefCell<Particle<'a>>>,
}

pub struct Event<'a> {
    pub event_number: u64,
    pub scale: f64,
    pub weight: f64,
    pub cross_section: f64, /* [pb] */
    pub cross_section_error: f64, /* [pb] */
    pub vertices: Vec<RefCell<Vertex<'a>>>,
    pub particles: Vec<RefCell<Particle<'a>>>,
}

fn encode_particle(p: &Particle<'_>) -> String {
    format!("P {} {} {} {} {} {} {} {} {} {} {} {}\n",
            p.barcode, p.pdg_id,
            p.momentum.x,
            p.momentum.y,
            p.momentum.z,
            p.momentum.t,
            p.generated_mass, p.status,
            0_f64, 0_f64, /* polarization */
            p.end_vertex.map(|v| v.borrow().barcode).unwrap_or(0),
            0, /* flow */
            )
}

fn encode_vertex(v: &Vertex<'_>) -> String {
    let orphan_particles_in = v.particles_in
        .iter()
        .filter(|p| p.borrow().production_vertex.is_none())
        .collect::<Vec<&&RefCell<_>>>();
    let mut res: String = format!("V {} {} {} {} {} {} {} {} {}\n",
            v.barcode,
            0, /* user defined id */
            0_f64, 0_f64, 0_f64, 0_f64, /* x, y, z, t */
            orphan_particles_in.len(),
            v.particles_out.len(),
            0, /* number of weights */
            );
    for p in orphan_particles_in {
        res += encode_particle(p.borrow().deref()).as_str()
    }
    for p in v.particles_out.iter() {
        res += encode_particle(p.borrow().deref()).as_str()
    }
    res
}

pub fn encode_event(e: &Event<'_>) -> String {
    let mut res: String = format!("E {} {} {} {} {} {} {} {} {} {} {} {} {}\nN 1 \"0\"\nU GEV MM\nC {} {}\n",
            e.event_number,
            -1, /* mpi */
            e.scale,
            0_f64, /* alpha QCD */
            0_f64, /* alpha QED */
            0, /* signal_process_id (like MSUB in pythia6) */
            e.vertices[0].borrow().barcode,
            e.vertices.len(),
            e.particles[0].borrow().barcode,
            e.particles[1].borrow().barcode,
            0, /* rng status */
            1, /* number of weights */
            e.weight,
            e.cross_section,
            e.cross_section_error,
            );
    for v in e.vertices.iter() {
        res += encode_vertex(v.borrow().deref()).as_str()
    }
    res
}
