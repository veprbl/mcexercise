pub mod constants {
    pub const ALPHA : f64 = 7.297352e-3;
    pub const GAMMA_Z : f64 = 2.4414; /* GeV. PDG has it 2.4952 */
    pub const G_F : f64 = 1.1663787e-5; /* GeV^-2 */
    pub const HC2 : f64 = 389379338.; /* GeV^2*pb */
    pub const M_Z : f64 = 91.1876; /* GeV */
    pub const SINTHETAW2 : f64 = 0.222246_f64; /* PDG has it 0.23126 at scale of M_Z in msbar */

    pub const PDG_ID_MU: i32 = 13;
    pub const PDG_ID_Z: i32 = 23;
    pub const PDG_ID_PROTON: i32 = 2212;
}

pub mod hepmc;
