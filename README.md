This is an implementation of the Monte Carlo codes for exercises from [arXiv:1412.4677](https://arxiv.org/abs/1412.4677).

The repository also contains a very rough Rust wrapper for LHAPDF6 and a similarily rough implementation of HepMC2 event serialization.

Here is another code: https://github.com/cbpark/PartonMCEx
