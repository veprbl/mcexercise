with import <nixpkgs> {};

let
  mcex = rustPlatform.buildRustPackage rec {
    name = "mcex";
    src = lib.sourceByRegex ../. [
      "^Cargo.lock$"
      "^Cargo.toml$"
      "^src$"
      "^src/.+$"
      "^lhapdf-sys$"
      "^lhapdf-sys/(Cargo.toml\|build\.rs\|src).*$"
      ];
    buildInputs = [ lhapdf ];
    cargoSha256 = "16dqd9yhrn8ync7ghf429qyyaswwfzhpw54khvgckpmz0bjl3pv6";
  };
  ex2_run = stdenv.mkDerivation {
    name = "ex2-run";
    buildInputs = [ rivet mcex lhapdf.pdf_sets.cteq6l1 ];
    phases = [ "buildPhase" ];
    buildPhase = ''
      ex2
      rivet output.hepmc -a MC_ZINC -a MC_ZINC_MU -a MC_ZINC_MU_BARE -a MC_MUONS
      install -Dm644 Rivet.yoda $out/output.yoda
    '';
  };
  sherpa_run = stdenv.mkDerivation {
    name = "sherpa-run";
    buildInputs = [ rivet sherpa lhapdf.pdf_sets.cteq6l1 ];
    phases = [ "buildPhase" ];
    run_dat = sherpa/Run.dat;
    buildPhase = ''
      cp "$run_dat" ./$(stripHash "$run_dat")
      Sherpa
      rivet out.hepmc2g -a MC_ZINC -a MC_ZINC_MU -a MC_ZINC_MU_BARE -a MC_MUONS
      install -Dm644 Rivet.yoda $out/output.yoda
    '';
  };
in
  stdenv.mkDerivation {
    name = "compare";
    buildInputs = [ rivet ];
    phases = [ "buildPhase" ];
    buildPhase = ''
      rivet-mkhtml -o $out ${ex2_run}/output.yoda:mcex ${sherpa_run}/output.yoda:sherpa
    '';
  }
