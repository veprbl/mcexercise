#include <string>

#include <LHAPDF/LHAPDF.h>

extern "C" {

const char* version() {
    static std::string ver = LHAPDF::version();
    return ver.c_str();
}

LHAPDF::PDF* mkPDF(const char* setname, int member) {
    return LHAPDF::mkPDF(setname, member);
}

double xfxQ2(LHAPDF::PDF* pdf, int id, double x, double q2) {
    return pdf->xfxQ2(id, x, q2);
}

double alphasQ2(LHAPDF::PDF* pdf, double q2) {
    return pdf->alphasQ2(q2);
}

void dropPDF(LHAPDF::PDF* pdf) {
    delete pdf;
}

}
