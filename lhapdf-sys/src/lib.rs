#[macro_use]
extern crate lazy_static;

use std::sync::Mutex;
use std::ffi::CString;

lazy_static! {
    static ref LHAPDF_LOCK: Mutex<()> = Mutex::new(());
}

mod wrapper {
    pub enum PDF {}
    extern "C" {
        pub fn version() -> *const ::std::os::raw::c_char;
        pub fn mkPDF(setname: *const ::std::os::raw::c_char,
                     member: ::std::os::raw::c_int) -> *mut PDF;
        pub fn xfxQ2(pdf: *mut PDF, id: ::std::os::raw::c_int,
                     x: f64, q2: f64) -> f64;
        pub fn alphasQ2(pdf: *mut PDF, q2: f64) -> f64;
        pub fn dropPDF(pdf: *mut PDF);
    }
}

pub struct PDF {
    _pdf: *mut wrapper::PDF,
}

impl PDF {
    pub fn new(pdf_name: &str, member: i32) -> Self {
        let pdf_name = CString::new(pdf_name).unwrap();
        let _guard = LHAPDF_LOCK.lock();
        let _pdf = unsafe { wrapper::mkPDF(pdf_name.as_ptr(), member) };
        PDF { _pdf }
    }

    pub fn xfx_q2(&self, pdg_id: i32, x: f64, q2: f64) -> f64 {
        unsafe {
            wrapper::xfxQ2(self._pdf, pdg_id, x, q2)
        }
    }

    pub fn alphas_q2(&self, q2: f64) -> f64 {
        unsafe {
            wrapper::alphasQ2(self._pdf, q2)
        }
    }
}

impl Drop for PDF {
    fn drop(&mut self) {
        unsafe {
            wrapper::dropPDF(self._pdf)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::ffi::{CString,CStr};

    #[test]
    fn test_wrapper_version() {
        let _version = unsafe { CStr::from_ptr(wrapper::version()) };
        let version = _version.to_str().unwrap();
        let mut chars = version.chars();
        assert_eq!(chars.next(), Some('6'));
        assert_eq!(chars.next(), Some('.'));
    }

    #[test]
    fn test_wrapper_pdf() {
        let _guard = LHAPDF_LOCK.lock();
        unsafe {
            let pdf_name = CString::new("cteq6l1").unwrap();
            let pdf = wrapper::mkPDF(pdf_name.as_ptr(), 0);

            assert!((wrapper::xfxQ2(pdf, 21, 0.5, 10.) - 0.0239530972683).abs() < 1e-5);
            assert!((wrapper::alphasQ2(pdf, 10.) - 0.280439478581).abs() < 1e-5);

            wrapper::dropPDF(pdf);
        }
    }

    #[test]
    fn test_xfx_q2() {
        let pdf = PDF::new("cteq6l1", 0);

        assert!((pdf.xfx_q2(21, 0.5, 10.) - 0.0239530972683).abs() < 1e-5);
    }

    #[test]
    fn test_alphas_q2() {
        let pdf = PDF::new("cteq6l1", 0);

        assert!((pdf.alphas_q2(10.) - 0.280439478581).abs() < 1e-5);
    }
}
