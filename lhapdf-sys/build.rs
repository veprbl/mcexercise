extern crate cc;

fn main() {
    cc::Build::new()
        .cpp(true)
        .file("src/wrapper.cxx")
        .compile("wrapper");

    println!("cargo:rustc-link-lib=LHAPDF");
}
